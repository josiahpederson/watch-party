from rest_framework.views import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from .serializers import (
    ErrorSerializer,
    WatchlistSerializer,
    WatchlistDetailSerializer,
    MultipleWatchlistsSerializer,
    WatchlistItemDetailSerializer,
)
from .models import Watchlist, UserVO


def prepare_json_response(
    status_code: int, data, serializer=ErrorSerializer, many=True
):
    if 200 <= status_code < 300:
        serialized_data = serializer(data, many=many)
        return Response(serialized_data.data, status=status_code)
    else:
        serialized_error = ErrorSerializer({"message": data})
        return Response(serialized_error.data, status=status_code)


@api_view(["GET", "POST"])
@permission_classes([IsAuthenticated])
def list_watchlists(request) -> Response:
    if request.method == "GET":
        try:
            # this will need to be changed to a decoded JWT
            owner = UserVO.objects.get(username=request.user.username)
            watchlists = Watchlist.objects.filter(owners=owner)
            return prepare_json_response(
                200,
                {"watchlists": watchlists},
                serializer=MultipleWatchlistsSerializer,
                many=False,
            )
        except Exception as e:
            return prepare_json_response(500, e, many=False)

    else:
        try:
            data = request.data
            username_list = [request.user.username]
            if data.get("owners"):
                username_list += data["owners"]
                del data["owners"]
            owners = [
                UserVO.objects.get(username=username) for username in username_list
            ]
            watchlist = Watchlist.objects.create(**data)
            [watchlist.owners.add(owner) for owner in owners]
            return prepare_json_response(
                200, watchlist, serializer=WatchlistSerializer, many=False
            )
        except Exception as e:
            print(e.__cause__)
            return prepare_json_response(500, e, many=False)


@api_view(["GET", "PUT", "DELETE"])
@permission_classes([IsAuthenticated])
def get_watchlist(request, pk: int) -> Response:
    try:
        owner = UserVO.objects.get(username=request.user.username)
        owned_watchlists = Watchlist.objects.filter(owners=owner)
    except UserVO.DoesNotExist:
        return prepare_json_response(
            404,
            f"Did not find UserVO with username: {request.user.username}",
            many=False,
        )

    if request.method == "GET":
        try:
            watchlist = owned_watchlists.get(id=pk)
            return prepare_json_response(
                200, watchlist, serializer=WatchlistDetailSerializer, many=False
            )

        except Watchlist.DoesNotExist:
            return prepare_json_response(
                500, f"You do not own a watchlist with id {pk}", many=False
            )

    elif request.method == "PUT":
        try:
            watchlist = owned_watchlists.get(id=pk)
        except Watchlist.DoesNotExist:
            return prepare_json_response(
                500, f"You do not own a watchlist with id {pk}", many=False
            )

        data = request.data
        if "owners" in data:
            try:
                add_owners = [
                    UserVO.objects.get(username=username)
                    for username in data["owners"].get("add", [])
                ]
                remove_owners = [
                    UserVO.objects.get(username=username)
                    for username in data["owners"].get("remove", [])
                ]
            except UserVO.DoesNotExist as e:
                return prepare_json_response(404, e, many=False)

            for new_owner in add_owners:
                watchlist.owners.add(new_owner)

            for remove_owner in remove_owners:
                watchlist.owners.remove(remove_owner)

        if "name" in data:
            name = data["name"]
            watchlist.name = name

        if "description" in data:
            description = data["description"]
            watchlist.description = description

        watchlist.save()
        return prepare_json_response(
            200, watchlist, serializer=WatchlistDetailSerializer, many=False
        )

    else:
        count, _ = owned_watchlists.filter(id=pk).delete()
        return Response({"deleted": count > 0}, status=202)


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def add_watchlist_item(request, watchlist_id: int, tmdb_id: int) -> Response:
    try:
        owner = UserVO.objects.get(username=request.user.username)
        watchlist = Watchlist.objects.filter(owners=owner).get(id=watchlist_id)
    except Exception as e:
        return prepare_json_response(404, e, many=False)

    added = watchlist.add_movie(tmdb_id)
    return Response({"added": added}, status=200)


@api_view(["GET", "PUT", "DELETE"])
@permission_classes([IsAuthenticated])
def get_watchlist_item(request, watchlist_id: int, watchlist_item_id: int) -> Response:
    try:
        owner = UserVO.objects.get(username=request.user.username)
        watchlist = Watchlist.objects.filter(owners=owner).get(id=watchlist_id)
        watchlist_item = watchlist.watchlist_items.get(id=watchlist_item_id)
    except Exception as e:
        return prepare_json_response(404, e, many=False)

    if request.method == "GET":
        return prepare_json_response(
            200, watchlist_item, serializer=WatchlistItemDetailSerializer, many=False
        )

    elif request.method == "PUT":
        interest = request.data.get("interest")
        if interest:
            updated = watchlist_item.update_interest(interest)
            if not updated:
                return prepare_json_response(
                    403, f"{interest} is out of range. (1-3)", many=False
                )
        if request.data.get("toggle_watched"):
            watchlist_item.update_watched()
        return prepare_json_response(
            200, watchlist_item, serializer=WatchlistItemDetailSerializer, many=False
        )

    else:
        removed = watchlist.remove_movie(watchlist_item.id)
        return Response({"removed": removed}, status=202)
