from django.test import TestCase
from movies_app.tests.data.factory import MovieFactory
from .data.factory import UserVOFactory, WatchlistFactory, WatchlistItemFactory
from django.urls import reverse
from django.contrib.auth.models import User
from watchlists_app.models import UserVO, Watchlist
import json


class TestListWatchlistView(TestCase):
    def setUp(self) -> None:
        user_vo = UserVOFactory()
        WatchlistFactory.create(owners=[user_vo])
        User.objects.create_user(username=user_vo.username, password="password")

    def test_get_watchlists_with_authenticated_user(self) -> None:
        user_vo = UserVO.objects.all()[0]
        logged_in = self.client.login(username=user_vo.username, password="password")
        self.assertTrue(logged_in)
        response = self.client.get(reverse("list_watchlists_url"))
        self.assertEqual(response.status_code, 200)
        data = response.data
        self.assertTrue(len(data) > 0)
        self.client.logout()

    def test_list_watchlists_unauthenticated(self) -> None:
        response = self.client.get(reverse("list_watchlists_url"))
        self.assertEqual(response.status_code, 403)

    def test_create_new_watchlist(self):
        user_vo = UserVO.objects.all()[0]
        self.client.login(username=user_vo.username, password="password")
        response = self.client.post(
            reverse("list_watchlists_url"),
            {"name": "name111", "description": "description111"},
            format="json",
        )
        self.assertEqual(200, response.status_code)
        data = response.data
        self.assertIsNotNone(data.get("name"))
        self.assertIsNotNone(data.get("description"))
        self.client.logout()

    def test_get_watchlist(self) -> None:
        user_vo = UserVO.objects.all()[0]
        desired_watchlist = user_vo.watchlists.all()[0]
        self.client.login(username=user_vo.username, password="password")
        response = self.client.get(f"/api/watchlists/{desired_watchlist.id}/")
        self.assertEqual(200, response.status_code)
        self.client.logout()

    def test_update_watchlist(self) -> None:
        user_vo = UserVO.objects.all()[0]
        second_owner = UserVOFactory()
        desired_watchlist = WatchlistFactory.create(owners=[user_vo])
        new_name = "new name"
        self.client.login(username=user_vo.username, password="password")

        # first request to add an owner and update name
        response = self.client.put(
            f"/api/watchlists/{desired_watchlist.id}/",
            data=json.dumps(
                {"name": new_name, "owners": {"add": [second_owner.username]}}
            ),
            content_type="application/json",
        )
        data = response.data
        self.assertEqual(200, response.status_code)
        self.assertTrue(len(data.get("owners")) >= 2)
        self.assertEqual(new_name, data.get("name"))

        # second request to remove an owner
        response = self.client.put(
            f"/api/watchlists/{desired_watchlist.id}/",
            data=json.dumps({"owners": {"remove": [second_owner.username]}}),
            content_type="application/json",
        )
        data = response.data
        self.assertEqual(200, response.status_code)
        self.assertTrue(len(data.get("owners")) < 2)
        Watchlist.objects.filter(id=desired_watchlist.id).delete()
        self.client.logout()

    def test_delete_watchlist(self) -> None:
        user_vo = UserVO.objects.all()[0]
        watchlist = WatchlistFactory.create(owners=[user_vo])
        self.client.login(username=user_vo.username, password="password")
        response = self.client.delete(f"/api/watchlists/{watchlist.id}/")
        self.assertEqual(202, response.status_code)
        self.assertTrue(response.data["deleted"])
        self.client.logout()

    def test_get_watchlist_unauthenticated(self) -> None:
        response = self.client.get(reverse("list_watchlists_url"))
        self.assertEqual(response.status_code, 403)


class TestAddAndRemoveWatchlistItemsViews(TestCase):
    def setUp(self) -> None:
        user_vo = UserVOFactory()
        watchlist = WatchlistFactory.create(owners=[user_vo])
        WatchlistItemFactory(watchlist=watchlist)
        User.objects.create_user(username=user_vo.username, password="password")

    def test_add_watchlist_item(self) -> None:
        movie = MovieFactory()
        user_vo = UserVO.objects.all()[0]
        desired_watchlist = user_vo.watchlists.all()[0]
        self.client.login(username=user_vo.username, password="password")
        response = self.client.post(
            f"/api/watchlists/{desired_watchlist.id}/add_movie/{movie.tmdb_id}/"
        )
        self.assertEqual(200, response.status_code)
        self.assertTrue(response.data.get("added"))
        self.client.logout()

    def test_remove_watchlist_item(self) -> None:
        user_vo = UserVO.objects.all()[0]
        desired_watchlist = user_vo.watchlists.all()[0]
        movie = desired_watchlist.watchlist_items.all()[0]
        self.client.login(username=user_vo.username, password="password")
        response = self.client.delete(
            f"/api/watchlists/{desired_watchlist.id}/movies/{movie.id}/"
        )
        self.assertEqual(202, response.status_code)
        self.client.logout()


class TestToggleWatchedAndInterestViews(TestCase):
    def setUp(self) -> None:
        user_vo = UserVOFactory()
        watchlist = WatchlistFactory.create(owners=[user_vo])
        WatchlistItemFactory(watchlist=watchlist)
        User.objects.create_user(username=user_vo.username, password="password")

    def test_toggle_watched_status(self) -> None:
        user_vo = UserVO.objects.all()[0]
        desired_watchlist = user_vo.watchlists.all()[0]
        movie = desired_watchlist.watchlist_items.all()[0]
        self.client.login(username=user_vo.username, password="password")
        response = self.client.put(
            f"/api/watchlists/{desired_watchlist.id}/movies/{movie.id}/",
            json.dumps({"toggle_watched": True}),
            content_type="application/json",
        )
        self.assertEqual(200, response.status_code)
        self.client.logout()

    def test_update_interest(self) -> None:
        user_vo = UserVO.objects.all()[0]
        desired_watchlist = user_vo.watchlists.all()[0]
        movie = desired_watchlist.watchlist_items.all()[0]
        self.client.login(username=user_vo.username, password="password")
        response = self.client.put(
            f"/api/watchlists/{desired_watchlist.id}/movies/{movie.id}/",
            json.dumps({"interest": 1}),
            content_type="application/json",
        )
        self.assertEqual(200, response.status_code)


class TestWatchlistItemView(TestCase):
    def test_watchlist_item_details_view(self) -> None:
        user_vo = UserVOFactory()
        watchlist = WatchlistFactory.create(owners=[user_vo])
        watchlist_item = WatchlistItemFactory(watchlist=watchlist)
        User.objects.create_user(username=user_vo.username, password="password")

        self.client.login(username=user_vo.username, password="password")
        response = self.client.get(
            f"/api/watchlists/{watchlist.id}/movies/{watchlist_item.id}/"
        )
        data = response.data
        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(data.get("id"))
        self.assertIsNotNone(data.get("movie"))
        self.assertIsNotNone(data.get("watchlist"))
