from django.db import models
from movies_app.models import Movie
from django.core.validators import (
    MinLengthValidator,
    MinValueValidator,
    MaxValueValidator,
)


class UserVO(models.Model):
    username = models.CharField(
        max_length=150,
        unique=True,
        blank=False,
        null=False,
        validators=[MinLengthValidator(1)],
    )
    email = models.EmailField(
        max_length=254,
        unique=True,
        blank=False,
        null=False,
        validators=[MinLengthValidator(5)],
    )

    def __str__(self):
        return self.username


class Watchlist(models.Model):
    owners = models.ManyToManyField("UserVO", related_name="watchlists")
    name = models.CharField(max_length=50, blank=False, null=False)
    description = models.CharField(max_length=200, blank=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.owners:
            owners_str = " & ".join([owner.username for owner in self.owners.all()])
            return f"{self.name} by {owners_str}"
        else:
            return f"{self.name}"

    class Meta:
        ordering = ["date_updated"]

    def add_movie(self, tmdb_id: int) -> bool:
        try:
            cur_watchlist_items = self.watchlist_items.all()
            for item in cur_watchlist_items:
                if tmdb_id == item.movie.tmdb_id:
                    return False

            movie = Movie.objects.get_or_save(tmdb_id)
            WatchlistItem.objects.create(movie=movie, watchlist=self)
            return True
        except Exception as e:
            print(e)
            return False

    def remove_movie(self, watchlist_item_id: int) -> bool:
        count, _ = self.watchlist_items.filter(id=watchlist_item_id).delete()
        return count > 0


class WatchlistItem(models.Model):
    movie = models.ForeignKey(
        "movies_app.Movie", related_name="watchlist_items", on_delete=models.CASCADE
    )
    watchlist = models.ForeignKey(
        "Watchlist", related_name="watchlist_items", on_delete=models.CASCADE
    )
    watched = models.BooleanField(default=False)
    interest = models.PositiveSmallIntegerField(
        default=2,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3),
        ],
    )
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.watchlist.name}: {self.movie.title}"

    class Meta:
        ordering = ["date_added"]

    def update_interest(self, interest: int) -> bool:
        if 1 <= interest <= 3:
            self.interest = interest
            self.save()
            return True
        else:
            return False

    def update_watched(self) -> bool:
        self.watched = not self.watched
        self.save()
        return True
