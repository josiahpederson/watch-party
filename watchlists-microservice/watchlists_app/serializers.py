from rest_framework import serializers
from .models import Watchlist, WatchlistItem, UserVO
from movies_app.serializers import MovieModelDetailSerializer


class ErrorSerializer(serializers.Serializer):
    message = serializers.CharField(max_length=300)


class UserVOSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserVO
        fields = [
            "username",
        ]


class WatchlistItemSerializer(serializers.ModelSerializer):
    movie = MovieModelDetailSerializer(read_only=True)

    class Meta:
        model = WatchlistItem
        fields = [
            "id",
            "watched",
            "interest",
            "date_added",
            "movie",
        ]
        depth = 1


class WatchlistSerializer(serializers.ModelSerializer):
    owners = UserVOSerializer(read_only=True, many=True)

    class Meta:
        model = Watchlist
        fields = [
            "id",
            "name",
            "description",
            "date_created",
            "date_updated",
            "owners",
        ]
        depth = 1


class WatchlistDetailSerializer(serializers.ModelSerializer):
    watchlist_items = WatchlistItemSerializer(many=True, read_only=True)
    owners = UserVOSerializer(many=True, read_only=True)

    class Meta:
        model = Watchlist
        fields = [
            "id",
            "name",
            "description",
            "date_created",
            "date_updated",
            "owners",
            "watchlist_items",
        ]
        depth = 2


class MultipleWatchlistsSerializer(serializers.Serializer):
    watchlists = WatchlistSerializer(many=True)
    depth = 2


class WatchlistItemDetailSerializer(serializers.ModelSerializer):
    movie = MovieModelDetailSerializer(read_only=True)
    watchlist = WatchlistSerializer(read_only=True)

    class Meta:
        model = WatchlistItem
        fields = [
            "id",
            "watched",
            "interest",
            "date_added",
            "movie",
            "watchlist",
        ]
        depth = 1
