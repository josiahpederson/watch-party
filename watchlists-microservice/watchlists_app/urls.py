from django.urls import path
from .views import (
    list_watchlists,
    get_watchlist,
    add_watchlist_item,
    get_watchlist_item,
)


urlpatterns = [
    path("watchlists/", list_watchlists, name="list_watchlists_url"),
    path("watchlists/<int:pk>/", get_watchlist, name="get_watchlist_url"),
    path(
        "watchlists/<int:watchlist_id>/add_movie/<int:tmdb_id>/",
        add_watchlist_item,
        name="add_watchlist_item_url",
    ),
    path(
        "watchlists/<int:watchlist_id>/movies/<int:watchlist_item_id>/",
        get_watchlist_item,
        name="get_watchlist_item_url",
    ),
]
