from rest_framework import serializers
from .models import Movie, Genre


class GenreSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Genre
        fields = [
            "name",
            "tmdb_id",
        ]


class MovieModelDetailSerializer(serializers.ModelSerializer):
    genres = GenreSerializer(many=True, read_only=True)

    class Meta:
        model = Movie
        fields = ["tmdb_id", "title", "runtime", "poster_path", "genres"]
        depth = 1


class MovieListSerializer(serializers.Serializer):
    tmdb_id = serializers.IntegerField()
    title = serializers.CharField(max_length=250)
    overview = serializers.CharField(max_length=2000)
    vote_count = serializers.IntegerField()
    vote_average = serializers.IntegerField()
    poster_path = serializers.CharField(max_length=299, allow_blank=True, default="")
    release_date = serializers.CharField(max_length=50, allow_blank=True, default="")


class ApiRequestMovieDetailSerializer(MovieListSerializer):
    tagline = serializers.CharField(max_length=300, allow_blank=True, default="")
    runtime = serializers.IntegerField()
    imdb_url = serializers.CharField(max_length=299, allow_blank=True, default="")
    genres = serializers.ListField()


class ErrorSerializer(serializers.Serializer):
    message = serializers.CharField(max_length=300)
