from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/users/", include("accounts_app.urls")),
    path("api/", include("accounts_app.auth.urls")),
    path("api/", include("rest_framework.urls", namespace="rest_framework")),
]
