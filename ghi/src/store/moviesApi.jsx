import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const moviesApi = createApi({
    reducerPath: 'movies',
    baseQuery: fetchBaseQuery({ baseUrl: `${process.env.REACT_APP_MOVIES}/api`}),
    endpoints: builder => ({
      getPopular: builder.query({
        query: () => "/movies/popular/",
      }),
      searchMovies: builder.query({
        query: (query) => (`/movies/search/${query}/`),
      }),
      getMovieDetails: builder.query({
        query: (tmdbId) => (`/movies/${tmdbId}/`),
      }),
    }),
  })

  export const { useGetPopularQuery, useSearchMoviesQuery, useGetMovieDetailsQuery } = moviesApi;
