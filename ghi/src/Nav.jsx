import {NavLink} from "react-router-dom"
import SearchBar from "./components/cards/SearchBar";

function Nav({movieQuery, setMovieQuery}) {
  return (
    <nav>
      <div>
        <ul>
          <li>
            <NavLink aria-current="page" to="/">Home</NavLink>
          </li>
          <li>
            <NavLink aria-current="page" to="/testhome">Test Home</NavLink>
          </li>
          <li>
            <SearchBar />
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Nav;
