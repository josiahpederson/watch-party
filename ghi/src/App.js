import './App.css';
import TestHome from './components/TestHome';
import { useGetPopularQuery, useSearchMoviesQuery } from './store/moviesApi';
import MovieCardList from './components/cards/MovieCardList';
import MovieDetailCard from './components/cards/MovieDetailCard';
import { useGetMovieDetailsQuery } from './store/moviesApi';
import {
  Route,
  BrowserRouter,
  Routes
} from "react-router-dom";
import Nav from './Nav';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div>
        <Routes>
          <Route index element={<MovieCardList query={ useGetPopularQuery } />} />
          <Route path="testhome" element={<TestHome />} />
          <Route path="search/:queryParam" element={<MovieCardList query={useSearchMoviesQuery} />} />
          <Route path="movies/:tmdbId" element={<MovieDetailCard query={useGetMovieDetailsQuery}/>} />
        </Routes>
      </div>
    </BrowserRouter>
  )
}

export default App;
