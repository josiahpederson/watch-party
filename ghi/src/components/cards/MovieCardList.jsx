import React from "react";
import MovieCard from "./MovieCard";
import Container from 'react-bootstrap/Container';
import { useParams } from "react-router-dom";


function MovieCardList({query}) {
  const { queryParam } = useParams()
  const { data, error, isLoading } = query(queryParam);

  if (isLoading) {
    return (
      <progress className="progress is-primary" />
    );
  }
  if (error) {
    console.log(error);
    return (
      <p>{error.error} --- {error.status}</p>
    );
  }
  return(
    <>
      {queryParam ? <h1>Search: {queryParam}</h1> : <h1>Popular Movies</h1>}
      <h3>{ error ? "ERROR!" : ""}</h3>
      <h3>{ data ? "" : "No movies found"}</h3>
      <Container fluid>

        {/* <Row>
          <Col>1</Col>
          <Col>2</Col>
          <Col>3</Col>
        </Row> */}

        { data.map(movie => (
            <MovieCard key={movie.tmdb_id} movie={movie} />
        ))}

      </Container>
    </>
  );
}

export default MovieCardList;
