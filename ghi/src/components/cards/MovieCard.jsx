import React from "react";
import { Link } from "react-router-dom";

function MovieCard({ movie }) {
  return (
    <div className="card">
      <Link to={`/movies/${movie.tmdb_id}`}>
        <h3 className="card-title">{movie.title} ({movie.release_date.slice(0,4)})</h3>
      </Link>
      {
        movie.vote_average > 0 ?
        <p className="card-body">{movie.vote_average}/10</p>
        :
        <p className="card-body">Not yet rated</p>
      }
    </div>
  );
}

export default MovieCard;
