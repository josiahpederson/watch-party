import React from "react";
import { useParams } from "react-router-dom";

const MovieDetailCard = ({query}) => {
  const movieIdentifier = useParams();
  const { data, error, isLoading } = query(movieIdentifier.tmdbId);

  if (isLoading) {
    return (
      <progress className="progress is-primary" />
    );
  }
  if (error) {
    console.log(error);
    return (
      <p>{error.error} --- {error.status}</p>
    );
  }
  console.log(data);
  return (
    <div className="container col-6">
      <img src={`https://image.tmdb.org/t/p/w500${data.poster_path}`} alt="poster" />
      <h1>{data.title}</h1>
      <h4>{data.tagline}</h4>
      <p>{data.overview}</p>
      <p>Released: {data.release_date}</p>
      <p>{data.runtime} minutes</p>
      {data.vote_average > 0 ?
        <p>Rating: {data.vote_average}/10 based on {data.vote_count} votes.</p>
      :
        <p>No ratings yet.</p>}
    </div>
  );
}

export default MovieDetailCard;
