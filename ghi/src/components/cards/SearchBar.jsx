import React from "react";
import { useNavigate } from 'react-router-dom';
import { useState } from "react";

const SearchBar = () => {
  const [query, setQuery] = useState("")

  const navigate = useNavigate();

  function getMovies(e) {
    e.preventDefault();
    return navigate(`/search/${query}`);
  }

  const updateQuery = (event) => {
    setQuery(() => event.target.value);
  }

  return (
    <form className="form" onSubmit={getMovies}>
      <input placeholder="Find a movie" onChange={updateQuery} value={query} name="query" type="text" minLength={1} maxLength={25} />
      <button>Search</button>
    </form>
  );
}

export default SearchBar;
