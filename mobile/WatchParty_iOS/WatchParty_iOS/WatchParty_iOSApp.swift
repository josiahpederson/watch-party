//
//  WatchParty_iOSApp.swift
//  WatchParty_iOS
//
//  Created by Josiah Pederson on 4/3/23.
//

import SwiftUI

@main
struct WatchParty_iOSApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
