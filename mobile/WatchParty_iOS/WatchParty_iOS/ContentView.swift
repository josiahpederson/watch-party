//
//  ContentView.swift
//  WatchParty_iOS
//
//  Created by Josiah Pederson on 4/3/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        MoviesListView()
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
