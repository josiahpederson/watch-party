//
//  MovieStructs.swift
//  watchParty
//
//  Created by Josiah Pederson on 4/3/23.
//

import Foundation

struct MovieBasics: Codable {
    var tmdb_id: Int
    var title: String
    var overview: String
    var vote_count: Int
    var vote_average: Int
    var poster_path: String
    var release_date: String
}
