//
//  MoviesListView.swift
//  watchParty
//
//  Created by Josiah Pederson on 4/3/23.
//

import SwiftUI
import Foundation

struct MoviesListView: View {
    @StateObject var vm: ViewModel
    
    init() {
        _vm = StateObject.init(wrappedValue: ViewModel())
    }
    
    var body: some View {
        VStack {
            Text("Movies")
                .font(.title)
            List(vm.movies, id: \.tmdb_id) { movie in
                VStack(alignment: .leading) {
                    Text(movie.title)
                        .font(.headline)
                    Text(movie.overview)
                }
            }
            .task {
                await vm.loadData()
            }
        }
    }
    
    @MainActor
    class ViewModel: ObservableObject {
        @Published var movies = [MovieBasics]()

        func loadData() async {
            guard let url = URL(string: "http://localhost:8004/api/movies/popular/") else {
                print("Invalid URL")
                return
            }
            do {
                let (data, _) = try await URLSession.shared.data(from: url)
                if let decodedResponse = try? JSONDecoder().decode([MovieBasics].self, from: data) {
                    movies = decodedResponse
                }
            } catch {
                print("Invalid data")
            }
        }
    }
}

struct MoviesListView_Previews: PreviewProvider {
    static var previews: some View {
        MoviesListView()
    }
}
