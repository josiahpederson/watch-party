# APIs

This page shows you the methods, paths and input/output schema of all APIs in Watch Party.

## Movie queries

### Search for a movie based off the title

Searching for a movie based on the title returns the top five results. A request is sent to the Redis caching microservice which will return the query results from its NoSQL database or from TMDB. Up to 20 results will be returned.

* **Method**: `GET`
* **Path**: /api/movies/search/

Input:

```json
{
  "query": "batman"
}
```

Output:

```json
[
	{
		"tmdb_id": 414906,
		"title": "The Batman",
		"overview": "In his second year of fighting crime, Batman uncovers corruption in Gotham City that connects to his own family while facing a serial killer known as the Riddler.",
		"vote_count": 7350,
		"vote_average": 7,
		"poster_path": "/74xTEgt7R36Fpooo50r9T25onhq.jpg",
		"release_date": "2022-03-04"
	},
	{
		"tmdb_id": 268,
		"title": "Batman",
		"overview": "Batman must face his most ruthless nemesis when a deformed madman calling himself \"The Joker\" seizes control of Gotham's criminal underworld.",
		"vote_count": 6722,
		"vote_average": 7,
		"poster_path": "/cij4dd21v2Rk2YtUQbV5kW69WB2.jpg",
		"release_date": "1989-06-23"
	}
]
```

### Get the top 20 trending movies

Make a request to this endpoint to receive basic info about the top 20 trending movies. A request is sent to the Redis caching microservice which will return the query results from its NoSQL database or from TMDB.

* **Method**: `GET`
* **Path**: /api/movies/popular/

Output:

```json
[
	{
		"tmdb_id": 76600,
		"title": "Avatar: The Way of Water",
		"overview": "Set more than a decade after the events of the first film, learn the story of the Sully family (Jake, Neytiri, and their kids), the trouble that follows them, the lengths they go to keep each other safe, the battles they fight to stay alive, and the tragedies they endure.",
		"vote_count": 4808,
		"vote_average": 7,
		"poster_path": "/t6HIqrRAclMCA60NsSmeqe9RmNV.jpg",
		"release_date": "2022-12-16"
	},
]
```

### Get details about a movie

To get the details of a desired movie, search for it by its unique numerical `tmdb_id`. A request is sent to the Redis caching microservice which will return the query results from its NoSQL database or from TMDB.

* **Method**: `GET`
* **Path**: /api/movies/<int:tmdb_id>/

Output:

```json
{
	"tmdb_id": 11,
	"title": "Star Wars",
	"overview": "Princess Leia is captured and held hostage by the evil Imperial forces in their effort to take over the galactic Empire. Venturesome Luke Skywalker and dashing captain Han Solo team together with the loveable robot duo R2-D2 and C-3PO to rescue the beautiful princess and restore peace and justice in the Empire.",
	"vote_count": 18186,
	"vote_average": 8,
	"poster_path": "/6FfCtAuVAW8XJjZ7eWeLibRLWTw.jpg",
	"release_date": "1977-05-25",
	"tagline": "A long time ago in a galaxy far, far away...",
	"runtime": 121,
	"imdb_url": "",
	"genres": [
		{
			"id": 12,
			"name": "Adventure"
		},
		{
			"id": 28,
			"name": "Action"
		},
		{
			"id": 878,
			"name": "Science Fiction"
		}
	]
}
```

## Genre queries

### List genres

This endpoint lists all genres. A request is sent to the movie Django microservice which queries it's PostgreSQL database.

* **Method**: `GET`
* **Path**: /api/genres/

Output:

```json
[
	{
		"id": 1,
		"name": "Action",
		"tmdb_id": 28
	},
]
```
### Get genre

This endpoint gets a desired genre from the id. A request is sent to the movie Django microservice which queries it's PostgreSQL database.

* **Method**: `GET`
* **Path**: /api/genres/<int:pk>/

Output:

```json
{
  "id": 1,
  "name": "Action",
  "tmdb_id": 28
}
```

## Watchlist endpoints
These are the endpoints in the `watchlists-microservice` that have to do with accessing watchlists. All these endpoints require authentication. The request examples for the docs were made by a username of `admin`.

### List user's watchlists

This endpoint lists all the watchlists that are owned by a user.

* **Method**: `GET`
* **Path**: /api/watchlists/

Output:

```json
{
	"watchlists": [
		{
			"id": 1,
			"name": "My First Watchlist",
			"description": "A bunch of movies I want to watch with friends",
			"date_created": "2023-01-24T18:14:44.469135Z",
			"date_updated": "2023-01-24T20:07:18.496666Z",
			"owners": [
				{
					"username": "admin"
				},
				{
					"username": "jinspins"
				},
				{
					"username": "jektono"
				}
			]
		},
		{
			"id": 4,
			"name": "Scary Movies Watchlist",
			"description": "Remember, it's just a movie.",
			"date_created": "2023-01-25T19:05:59.344024Z",
			"date_updated": "2023-01-25T19:12:25.687450Z",
			"owners": [
				{
					"username": "admin"
				},
				{
					"username": "jinspins"
				},
				{
					"username": "tarwhoop"
				}
			]
		}
	]
}
```

### Create watchlist

Create a new watchlist owned by one or many users. The user who makes the request is automatically added as an owner but additional owners can be supplied with the optional `owners` key/value pair as demonstrated below.

* **Method**: `POST`
* **Path**: /api/watchlists/

Input:

```json
{
	"owners": ["tarwhoop", "pinsjins"],
	"name": "Upcoming Releases Watchlist",
	"description": "2024 lineup is looking good!"
}
```

Output:

```json
{
	"id": 5,
	"name": "Upcoming Releases Watchlist",
	"description": "2024 lineup is looking good!",
	"date_created": "2023-01-25T20:32:25.884974Z",
	"date_updated": "2023-01-25T20:32:25.885051Z",
	"owners": [
		{
			"username": "admin"
		},
		{
			"username": "pinsjins"
		},
		{
			"username": "tarwhoop"
		}
	]
}
```

### Get watchlist details

Get the details pertaining to a specified watchlist

* **Method**: `GET`
* **Path**: /api/watchlists/<watchlist_id:int>/

Output:

```json
{
	"id": 4,
	"name": "Classic Movies",
	"description": "These came before my time",
	"date_created": "2023-01-25T19:05:59.344024Z",
	"date_updated": "2023-01-25T19:12:25.687450Z",
	"owners": [
		{
			"username": "admin"
		},
		{
			"username": "jinspins"
		},
		{
			"username": "tarwhoop"
		}
	],
	"watchlist_items": [
		{
			"id": 11,
			"watched": false,
			"interest": 1,
			"date_added": "2023-01-25T19:23:52.626248Z",
			"movie": {
				"tmdb_id": 335,
				"title": "Once Upon a Time in the West",
				"runtime": 166,
				"poster_path": "/qbYgqOczabWNn2XKwgMtVrntD6P.jpg",
				"genres": [
					{
						"name": "Drama",
						"tmdb_id": 18
					},
					{
						"name": "Western",
						"tmdb_id": 37
					}
				]
			}
		}
	]
}
```

### Update watchlist

Update the `name`, `description`, and `owners` of a watchlist. All keys are optional. If you include the `owners` key, you must format the nested dictionary with one or both of the following keys: `add` and `remove`. This indicates which users are being added and removed from the watchlist.

* **Method**: `PUT`
* **Path**: /api/watchlists/<watchlist_id:int>/

Input:

```json
{
	"name": "Chic Flics",
	"description": "Guilty pleasure",
	"owners": {
		"add": ["tarwhoop"],
		"remove": ["pinsjins"]
	}
}
```

Output:

```json
{
	"id": 6,
	"name": "Chic Flics",
	"description": "Guilty pleasure",
	"date_created": "2023-01-25T20:32:25.884974Z",
	"date_updated": "2023-01-25T20:32:25.885051Z",
	"owners": [
		{
			"username": "admin"
		},
		{
			"username": "tarwhoop"
		},
	],
	"watchlist_items": []
}
```
### Delete watchlist

Delete a watchlist and all associated WatchlistItems. If a watchlist was not found with the specified `watchlist_id` the deleted boolean will return `false`.

* **Method**: `DELETE`
* **Path**: /api/watchlists/<watchlist_id:int>/

Output:

```json
{
	"deleted": true
}
```

### Add movie to Watchlist

Add a movie to an existing watchlist. NOTE: The movie is referenced by its tmdb_id number.

* **Method**: `P0ST`
* **Path**: /api/watchlists/<watchlist_id:int>/add_movie/<tmdb_id:int>/

Output:

```json
{
	"added": true
}
```

### Get Watchlist item details
This endpoint supplies all the details pertaining to a movie on a watchlist. NOTE: Movies that are part of a watchlist are NOT referenced by their tmdb_id numbers. They are referenced by the id number of the WatchlistItem that points to a movie.

* **Method**: `GET`
* **Path**: /api/watchlists/<watchlist_id:int>/movies/<watchlist_item_id:int>/

Output:
```json
{
	"id": 15,
	"watched": true,
	"interest": 1,
	"date_added": "2023-01-25T21:27:49.157237Z",
	"movie": {
		"tmdb_id": 12,
		"title": "Finding Nemo",
		"runtime": 100,
		"poster_path": "/7DPFmp0q5RC1p582SdIuZ4KBF2.jpg",
		"genres": [
			{
				"name": "Animation",
				"tmdb_id": 16
			},
			{
				"name": "Family",
				"tmdb_id": 10751
			}
		]
	},
	"watchlist": {
		"id": 4,
		"name": "Classics",
		"description": "These came before my time",
		"date_created": "2023-01-25T19:05:59.344024Z",
		"date_updated": "2023-01-25T21:01:24.216304Z",
		"owners": [
			{
				"username": "admin"
			},
			{
				"username": "jinspins"
			},
			{
				"username": "tarwhoop"
			}
		]
	}
}
```

### Update a WatchlistItem (interest and watched status)
Toggle whether a movie has been watched or not and update interest level in a movie.

* **Method**: `PUT`
* **Path**: /api/watchlists/<watchlist_id:int>/movies/<watchlist_item_id:int>/

Output:

```json
{
	"id": 1,
	"watched": true,
	"interest": 3,
	"date_added": "2023-01-30T20:37:00.639771Z",
	"movie": {
		"tmdb_id": 11,
		"title": "Star Wars",
		"runtime": 121,
		"poster_path": "/6FfCtAuVAW8XJjZ7eWeLibRLWTw.jpg",
		"genres": [
			{
				"name": "Action",
				"tmdb_id": 28
			},
			{
				"name": "Adventure",
				"tmdb_id": 12
			},
			{
				"name": "Science Fiction",
				"tmdb_id": 878
			}
		]
	},
	"watchlist": {
		"id": 1,
		"name": "Classics",
		"description": "These came before my time",
		"date_created": "2023-01-30T20:35:50.209695Z",
		"date_updated": "2023-01-30T20:36:37.227123Z",
		"owners": [
			{
				"username": "jinspins"
			},
			{
				"username": "admin"
			},
			{
				"username": "tarwhoop"
			}
		]
	}
}
```

### Remove movie from Watchlist

Removie a movie from an existing watchlist.

* **Method**: `DELETE`
* **Path**: /api/watchlists/<watchlist_id:int>/movies/<watchlist_item_id:int>/

Output:

```json
{
	"removed": true
}
```
