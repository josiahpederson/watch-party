from django.db import models
from django.core.validators import MinLengthValidator


class UserVO(models.Model):
    username = models.CharField(
        max_length=150,
        unique=True,
        blank=False,
        null=False,
        validators=[MinLengthValidator(1)],
    )
    email = models.EmailField(
        max_length=254,
        unique=True,
        blank=False,
        null=False,
        validators=[MinLengthValidator(5)],
    )

    def __str__(self) -> str:
        return self.username


class WatchParty(models.Model):
    movie = models.IntegerField(blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self) -> str:
        if self.attendees.all():
            attendees_str = " & ".join(
                [attendee.attendee.username for attendee in self.attendees.all()]
            )
            return f"Watchparty on {self.date} by {attendees_str}"
        else:
            return f"Watch Party on {self.date}"


class WatchPartyAttendee(models.Model):
    attendee = models.ForeignKey(
        "UserVO", related_name="watch_parties", on_delete=models.PROTECT
    )
    watch_party = models.ForeignKey(
        "WatchParty", related_name="attendees", on_delete=models.CASCADE
    )

    def __str__(self) -> str:
        return f"{self.attendee.username} attendee"
