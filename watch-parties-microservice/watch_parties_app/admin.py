from django.contrib import admin
from .models import UserVO, WatchParty, WatchPartyAttendee


class UserVOAdmin(admin.ModelAdmin):
    pass


class WatchPartyAdmin(admin.ModelAdmin):
    pass


class WatchPartyAttendeeAdmin(admin.ModelAdmin):
    pass


admin.site.register(UserVO, UserVOAdmin)
admin.site.register(WatchParty, WatchPartyAdmin)
admin.site.register(WatchPartyAttendee, WatchPartyAttendeeAdmin)
